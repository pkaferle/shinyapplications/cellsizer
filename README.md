# CellSizer

https://petrakaferle.shinyapps.io/CellSizer/

A machine counts cells in a range of size of approx. 3-10 um. This range is user-defined and is divided in 256 bins, for which the cell counts are reported. From the data we calculate the occupied volume of the cells in each bin, which is an alternative way to present the findings. Processing time in Excel takes about half an hour per dataset (=biological sample), while individual experiment can consist of up to 10 samples.

The aim was to automate the procedure and save researcher's time. The app reads the file in the exact same format as is output from cell counting instrument, so there's no need for pre-processing. It calculates total volume occupied by cells and visualizes cell count and volume on different plots. User can also select the additional threshold and get the information on how many cells are above or below. Additional features are the capability of comparing multiple files from the same folder or plot only the selected ones after initial preview. User also has some liberty over choosing colours from pre-defined colour palette and resizing y-axis to match the form of previous reports. The legend is generated automatically, based on names of files, but it can be changed to suite user's need. The app generates downloadable summary.   

![Capt_CellSizer](/uploads/d7e48817b90ec9260107a37ba5a1583a/Capt_CellSizer.PNG)